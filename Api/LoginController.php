<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\API\BaseController as BaseController;

use App\Models\User;
use Illuminate\Http\Request;
use  Validator ;
use Illuminate\Support\Facades\Auth;  //added

class LoginController extends  BaseController
{

    public function login(Request $request)
    {
        $loginData =    Validator::make($request->all(), [
            'email' => 'email|required',
            'password' => 'required'
        ] );

        if ($loginData -> fails()) {
            return $this->sendError('error validation', $loginData->errors());
        }
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials)) {
            return $this->sendError('Invalid credentials', $loginData->errors());
        }

        $user = $request->user();
        $success['token'] = $user->createToken('authToken')->accessToken;
        $success['name'] = $user->name;
        $success['email'] = $user->email;


        return $this->sendResponse($success , 'User login succesfully');

    }
}
